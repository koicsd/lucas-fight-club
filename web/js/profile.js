/**
 * Created by Zsori Lukacs Peter on 2016.06.24..
 */

/* Profile rész: */
(function(app){

    /* Felültetjük a dolgokat winddow load event-re,
     * hogy mindegy legyen, hova rakod a script hivatkozást a HTML-ben */
    window.addEventListener('load', init);  // addEventListener, event neve sztringként, on előtag nélkül, a fv-re név szerint hivatkozhatsz

    function init(event) {  // a meghivatkozott függvény egy event objektumot kap paraméterként
        /* Minden sztatikus tartalom betöltődött, jöhet a buli */
        /* Egy 'ProfileLogic' nevű objektum behelyezésével jelezzük, hogy az init függvény meghívódott. */
        app.ProfileLogic = app.ProfileLogic || {};
        /* Először lekérdezzük a profilt a szervlettől*/
        getProfile();
        /* Majd a logout függvény meghívását rákötjük a megfelelő gombra */
        document.getElementById('logout-btn').addEventListener('click', function (event) { logout(); });  // akár helyben kifejtett anonim fv is átveheti az event-et, a kívánt fv innen is meghívható
    }

    /* getting profile */
    function getProfile(){
        displayError('Waiting for getting profile from server...');
        /* a profil lekérdezését a main.js-ben lévő startRequest függvény meghívásával indítjuk */
        /* paraméterek:
            HTTP-metódus: GET
            URL: servlet-URL + '/profile'
            kész-JSON objektum feldolgozófüggvénye: processGetResponse
            küldendő objektum: nincs
            JSON-parse hiba esetén meghívandó: signParseError
            nem 200-as status-kód esetén meghívandó: signRequestFailure
         */
        app.Ajax.startRequest('GET', app.Config.servletUrl + '/profile', processGetResponse, undefined,
            signParseError, signRequestFailure);
    }

    function processGetResponse(response) {
        /* a felparszolt JSON adat feldolgozása */
        if (!!response.error && (!response.error.code || !response.error.message))
        {
            /* ha van 'error' a válaszban, de annak nincs 'code'-ja vagy 'message'-e, akkor rossz a szerver */
            console.log('Invalid answer from server:\n' + response);
            displayError('Invalid answer from server. See console.');
            return;  /* vége, nem kell else */
        }
        if (!response.error && (!response.email || !response.profile || !response.profile.name || !response.profile.age))
        {
            /* ha nincs 'error' a válaszban, de nincs 'email' vagy 'profile', vagy a 'profile'-nak nincs 'name'-je vagy 'age'-e: szintén rossz a szerver. */
            console.log('Invalid answer from server:\n' + response);
            displayError('Invalid answer from server. See console.');
            return;  /* vége, nem kell else */
        }
        if (!!response.error)
        {
            /* Ha ezek után még van 'error', azt megjelenítjük */
            if (response.error.code == 4) {  // not logged in
                /* Ha nincs bejelentkezve, alert-et dobunk, majd a login-oldalra küldjük. */
                alert('Error 4:\n' + response.error.message);
                window.location = 'index.html'; // redirection to login-page
            }
            displayError('Error ' + response.error.code + ':<br/>'
                + response.error.message);
            return;  /* vége, nem kell else */
        }
        /* megjelenítjük a profilt */
        displayProfile(response);
        /* és beélesítjük a kapcsolatkezelő függvényeket -- lásd lent */
        setSessionHandler();
    }

    function signParseError(exception, responseText)
    {
        /* JSON parszolás hibájának jelzése */
        displayError('Sorry. We cannot parse server-answer. See console.');
    }

    function signRequestFailure(statusCode)
    {
        /* nem 200-as status jelzése */
        displayError('HTTP-Request failed, see status code on console.');
    }

    function displayProfile(response) {
        /* profil tényleges megjelenítése */
        document.getElementById('error-panel').hidden = true;
        document.getElementById('error-message').innerHTML = '';
        document.getElementById('email-textbox').innerHTML = response.email;
        document.getElementById('name-textbox').innerHTML = response.profile.name;
        document.getElementById('age-textbox').innerHTML = response.profile.age;
        document.getElementById('content').style = "display: block";
    }

    function displayError(messageHTML) {
        /* a hibaüzenetek tényleges megjelenítése */
        document.getElementById('content').style = "display: none;";
        document.getElementById('email-textbox').innerHTML = '';
        document.getElementById('name-textbox').innerHTML = '';
        document.getElementById('age-textbox').innerHTML = '';
        document.getElementById('error-message').innerHTML = messageHTML;
        document.getElementById('error-panel').hidden = false;
    }

    /* logout */
    function logout(){
        /* most is a main.js startRequest függvényét használjuk */
        /* paraméterek:
         HTTP-metódus: POST
         URL: servlet-URL + '/logout'
         kész-JSON objektum feldolgozófüggvénye: processLogoutResponse
         küldendő objektum: nincs
         JSON-parse hiba vagy nem 200-as status-kód esetén meghívandó: nincs
         */
        app.Ajax.startRequest('POST', app.Config.servletUrl + '/logout', processLogoutResponse);
    }

    function processLogoutResponse(response) {
        if (!!response.error)
        {
            /* ha 'error' van... */
            if (!response.error.code || !response.error.message) {
                /* de nincs 'code' vagy 'message' az 'error'-ban, rossz a szerver */
                console.log('Invalid answer from server:\n' + response);
                alert('Invalid answer from server.');
            }
            else {
                /* ha szabályos a hiba-JSON */
                if (response.error.code == 4) {  // not logged in
                    /* és az a gond, hogy nincs bejelentkezve, */
                    /* először a profil helyén hibaüzenet megjelenítése */
                    displayError('Error ' + response.error.code + ':<br/>'
                        + response.error.message);
                    /* majd leállítjuk a kapcsolatkezelő időzítőket */
                    unsetSessionHandler();
                    /* végül alert után a login oldalra irányítjuk a delikvenst */
                    alert('Error ' + response.error.code + ':\n' + response.error.message);
                    window.location = 'index.html';  // redirection to login page
                }
                else {
                    /* ha pedig nem autentikációs gondja van, egy egyszerű alert is elég */
                    alert('Error ' + response.error.code + ':\n' + response.error.message);
                }
            }
            return;  /* vége, nem kell else */
        }
        /* ha nincs 'error', feltehetjük, hogy a kijelentkezés sikeres, leállítjuk az időzítőket, és átirányítjuk a login oldalra */
        displayError('Logout successful.');  // de előbb biztos, ami biztos, a profilt egy nyugtázó üzenetre cseréljük
        unsetSessionHandler();
        window.location = 'index.html';
    }

    /* keeping session alive and automatic logout -- kapcsolat életben tartása és automatikus kijelentkezés */
    var keepaliveInterval = 30000;  // dummy-request sent after this interval (ms) repeatedly -- ennyi ezredmásodperc időközönként küld a js dummy-request-et
    var idleTimeout = 300000;  // after this time (ms) of idle js itself logs out -- ennyi ezredmásoperc tétlenség után a js maga léptet ki
    var keepaliveTimer;  // timer for dummy-request -- időzítő a dummy-request-hez
    var logoutTimer;  // timer for automatic logout -- időzítő az automatikus kilépéshez

    function setSessionHandler() {
        /* kapcsolatkezelők beélesítése */
        /* először létrehozzuk és beprogramozzuk a két időzítőt */
        logoutTimer = setTimeout( handleUserInactivity, idleTimeout);  // név szerint rakunkk fel egy külön kifejtett függvényt, ami meghívja a szükséges függvényeket
        keepaliveTimer = setInterval(function (event) { keepAlive(); }, keepaliveInterval);  // itt az egyetlen függvényt helyben kifejtett anonim függvénnyel hívjuk meg
        /* azután felültetjük a user-aktivitást kezelő függvényt a megfelelő eseményekre */
        document.addEventListener('mouseup', handleUserActivity);
        document.addEventListener('mousedown', handleUserActivity);
        document.addEventListener('mousemove', handleUserActivity);
        document.addEventListener('keyup', handleUserActivity);
        document.addEventListener('keydown', handleUserActivity);
        document.addEventListener('focus', handleUserActivity);
    }

    function unsetSessionHandler() {
        /* kapcsolatkezelők kiiktatása */
        /* először leszedjük az aktivitás-kezelőt az eseményekről */
        document.removeEventListener('mouseup', handleUserActivity);
        document.removeEventListener('mousedown', handleUserActivity);
        document.removeEventListener('mousemove', handleUserActivity);
        document.removeEventListener('keyup', handleUserActivity);
        document.removeEventListener('keydown', handleUserActivity);
        document.removeEventListener('focus', handleUserActivity);
        /* majd leállítjuk és eldobjuk az időzítőket */
        clearInterval(keepaliveTimer);
        keepaliveTimer = undefined;
        clearTimeout(logoutTimer);
        logoutTimer = undefined;
    }

    function handleUserActivity(event) {
        /* a felhasználói aktivitást kezelő függvény */
        /* a kijelentkezési időzítőt leállítjuk, majd új időzítőt programozunk be helyette */
        clearTimeout(logoutTimer);
        logoutTimer = setTimeout( handleUserInactivity, idleTimeout);
    }

    function handleUserInactivity(event) {
        /* a logout időzítő először is írja ki a profil helyére, hogy túl régóta inaktív */
        displayError('You have been inactive for a long time.<br/>Automatic logout...');
        /* majd kapcsolja ki az időzítőket */
        unsetSessionHandler();
        /* végül próbáljon meg kijelentkezni */
        logout();
    }

    function keepAlive() {
        /* a kapcsolat életben tartása Ajax kéréssel történik a már megszokott függvénnyel */
        /* paraméterek:
         HTTP-metódus: GET
         URL: servlet-URL + '/keepalive'
         kész-JSON objektum feldolgozófüggvénye: processKeepaliveResponse
         küldendő objektum: nincs
         JSON-parse hiba vagy nem 200-as status-kód esetén meghívandó: üres függvények, hogy ne ugráljanak fel a main.js alert-jei
         */
        app.Ajax.startRequest('GET', app.Config.servletUrl + '/keepalive', processKeepaliveResponse, undefined,
            function (event) { }, function (event) { });
    }

    function processKeepaliveResponse(response) {
        if (!!response.error && (!response.error.code || !response.error.message))
        {
            /* ha van 'error' a válaszban, de annak nincs 'code'-ja vagy 'message'-e, akkor rossz a szerver */
            console.log('Invalid answer from server:\n' + response);
            return;  /* vége, nem kell else */
        }
        if (!!response.error) {
            /* ha van értelmes hibaüzenet, kiírjuk a konzolra (de alert nem kell) */
            console.log('Error ' + response.error.code + ':\n'
                + response.error.message);
            if (response.error.code == 4) {  // not logged in, session may have been invalidated
                /* persze ha a hibakód szerint eleve nincs is bejelentkezve, akkor szerver oldali session-timeout lehet */
                /* ekkor a profil helyén hibaüzenetet jelenítünk meg */
                displayError('Error ' + response.error.code + ':<br/>'
                    + response.error.message);
                /* majd kikapcsoljuk a kapcsolatkezelőt */
                unsetSessionHandler();
                /* végül mégis alert */
                alert('Your session has been invalidatad!');
                /* és irány a login oldal */
                window.location = 'index.html';  // redirection to login
            }
        }
        /* ha minden rendben volt, nincs további teendő */
    }
})(window.FightClubApp = window.FightClubApp || {});