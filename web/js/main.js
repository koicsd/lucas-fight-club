/**
 * Created by Zsori Lukacs Peter on 2016.06.22..
 */

/* Config */
(function (app) {
    /* Egy 'Config' objektumba beültetünk bizonyos globális konstansokat, hogy azt a többi javascript kód is lássa */
    app.Config = app.Config || {};
    /* Egészen pontosan egyetlen konstanst, éspedig a servlet URL-jét */
    app.Config.servletUrl = '/rest';
})(window.FightClubApp = window.FightClubApp || {});

/* AJAX */
(function (app) {
    /* Egy 'Ajax' objektum elhelyezésével jelezzük, hogy a fájl meghivatkozódott a HTML-ből (a függvény meghívta önmagát) */
    /* Az objektumon belül közzé tesszük a modul két függvényét, hogy azt a többi javascript kód is lássa */
    app.Ajax = app.Ajax || {};
    app.Ajax.startRequest = startRequest;
    app.Ajax.handleRequset = handleRequest;

    /* starting AJAX-request -- az AJAX-kérések indítófüggvénye */
    function startRequest(method, url, processorFcn, object, parseErrAlertFcn, requestFailAlertFcn){
        /* Paraméterek:
             method: a HTTP-metódus, 'GET' vagy 'POST'
             url: az URL, ahova a kérést küldeni kell
             processorFcn: a felparszolt válasz JSON objektumot tovább feldolgozó függvény
             object: az objektum, amit a kéréssel ki kell küldeni a szervernek, hogy az válaszolhasson
             parseErrAlertFcn: a JSON-parse elbukásakor meghivandó vüggvény, paraméterei a kivétel objektum és a JSON sztring
             requestFailAlertFcn: nem 200-as status-kóddal végződő válasz esetén meghívandó függvény, paramétere a status-kód
        */
        /* a jól ismert dal: új request, readyStateChange függvény beállítása, megnyitás, végül küldés */
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function(event) {  // helyben kifejtett anonim fv, mely átveszi az event-et, a kívánt fv-t most innen hívjuk meg
            handleRequest(event.target, processorFcn, parseErrAlertFcn, requestFailAlertFcn);  // az event.target maga a request, akinek az eseményére épp feliratkozunk, plussz a feldolgozó és a hibajelző függvényeket is átadjuk
        };
        httpRequest.open(method, url);  // nyitás a metódusnévvel és az URL-lel
        if (!!object)  // ha van küldendő objektum, azt JSON-sztringgé alakítva elküldjük
            httpRequest.send(JSON.stringify(object));
        else  // ha nincs, akkor üres küldés
            httpRequest.send();
        /* végül a konzolon is jelezzük a kérést */
        console.log('HTTP-' + method + '-Request sent to:\n' + url);
    }

    /* handling readyStateChange -- az AJAX-kérés állapotváltozását kezelő függvény */
    function handleRequest(httpRequest, processorFcn, parseErrAlertFcn, requestFailAlertFcn) {
        /* Paraméterek:
         httpRequest: a reqest, akit épp kezelni akarunk
         processorFcn: a felparszolt válasz JSON objektumot tovább feldolgozó függvény
         parseErrAlertFcn: a JSON-parse elbukásakor meghivandó vüggvény, paraméterei a kivétel objektum és a JSON sztring
         requestFailAlertFcn: nem 200-as status-kóddal végződő válasz esetén meghívandó függvény, paramétere a status-kód
         */
        if (httpRequest.readyState === XMLHttpRequest.DONE) {  // a folyamat véget ért, lássuk a status-kódot
            if (httpRequest.status === 200) {  // ha 200-as, akkor kaptunk választ
                /* ujjongunk a konzolon */
                console.log('HTTP-Request successful.');
                try  {
                    /* Megpróbáljuk a JSON-sztringet parszolni */
                    var response = JSON.parse(httpRequest.responseText);
                    /* Ha sikerült, meghívjuk rá a tovább feldolgozó függvényt */
                    processorFcn(response);
                }
                catch (exc) {
                    /* Ha nem sikerül a JSON feldolgozása, konzolon jelezzük a kivételt */
                    console.log('Exception of type ' + typeof(exc) + ' while parsing JSON response:\n' +
                        (exc.stack ? exc.stack : exc.toString()) +
                        '\nResponse text:\n' + httpRequest.responseText);
                    if (!!parseErrAlertFcn)
                        /* Majd ha van meghívandó függgvény, meghívjuk azt */
                        parseErrAlertFcn(exc, httpRequest.responseText);
                    else
                        /* Ha nincs, alert */
                        alert('Sorry. We cannot parse server-answer. See console.');
                }
            }
            else {  // ha nem 200 a status-kód, elbukott a request
                /* Bejelentjük a bukást a konzolon */
                console.log('HTTP-Request failed with code: ' + httpRequest.status);
                if (!!requestFailAlertFcn)
                    /* Ha van meghivandó függvény, meghívjuk azt */
                    requestFailAlertFcn(httpRequest.status);
                else
                    /* Ha nincs függvény, alert */
                    alert('HTTP-Request failed, see status code on console.');
            }
        }
    }
})(window.FightClubApp = window.FightClubApp || {});