/**
 * Created by Zsori Lukacs Peter on 2016.06.24..
 */

/* Login rész: */
(function(app){
    /* Felültetjük a dolgokat winddow load event-re,
     * hogy mindegy legyen, hova rakod a script hivatkozást a HTML-ben */
    window.addEventListener('load', init);  // addEventListener, event neve sztringként, on előtag nélkül, a fv-re név szerint hivatkozhatsz

    function init(event) {  // a meghivatkozott függvény egy event objektumot kap paraméterként
        /* Minden sztatikus tartalom betöltődött, jöhet a buli */
        /* Egy 'ProfileLogic' nevű objektum behelyezésével jelezzük, hogy az init függvény meghívódott. */
        app.LoginLogic = app.LoginLogic || {};
        /* a login függvényt ráültetjük a form-submit eseményre */
        document.getElementById('form').addEventListener('submit', login);
    }

    function login(event){
        /* bejelentkezés */
        /* először összegyűjtjük egy objektumba az adatokat */
        var loginData = {
            email: document.getElementById('email').value,
            password: document.getElementById('password').value
        };
        /* majd a main.js startRequest függvényével indítjuk a kérést */
        /* paraméterek:
            HTTP-metódus: GET
            URL: servlet-URL + '/profile'
            kész-JSON objektum feldolgozófüggvénye: processGetResponse
            küldendő objektum: az előbb összeszeállított 'loginData'
            hiba esetén meghívandó függvények: nincsenek, marad a main.js alapértelmezett alert-logikája
         */
        app.Ajax.startRequest('POST', app.Config.servletUrl + '/login', processLoginResponse, loginData);
    }

    function processLoginResponse(response) {
        /* a felparszolt json feldolgozása */
        if (!!response.error)
        {
            /* ha 'error' van... */
            if (!response.error.code || !response.error.message) {
                /* de nincs 'code' vagy 'message' az 'error'-ban, rossz a szerver */
                console.log('Invalid answer from server:\n' + response);
                alert('Invalid answer from server. See console.');
            }
            else
            /* amúgy alert-eljük a hibakódot és az üzenetet */
                alert('Error ' + response.error.code + ':\n' + response.error.message);
            return;  /* vége, nem kell else */
        }
        /* ha nincs 'error', feltehetjük, hogy a beelentkezés sikeres, s átirányítjuk a profil oldalra */
        window.location = 'profile.html';
    }
})(window.FightClubApp = window.FightClubApp || {});