# Hello!

This is a simple webapp to demonstrate
how to handle login-logout procedure.

My features:  
- On **server-side** there is a ***restful*** servlet.  
- On **client-side**, there is a static **login-page and profile-page**.  
- From **login-page**,
  a *JavaScript* logic **sends password via *AJAX* to server,
  and** if server accepts it, JS **redirects to the profile page**.  
- On **profile-page, *JS* fetches profile-data from server**
  as soon as profile-page has been loaded.  
- It **maintains timer to send connection-keep-alive requests**
  in every 30 seconds while User is active.  
- **If User is inactive** for 5 minutes, *JavaScript* **logs out** itself.  
- Of course, **on server-side** there is a 60-second **session-timeout**.  
- **If profile-page is loaded when not logged in,
  server refuses to send** profile-**data,
  and profile-side** *JavaScript* **redirects to login-page**.

Note:  
To check-out my project, you need 2 dependencies:  
- You need [*Tomcat*](http://tomcat.apache.org/)
  with its built-in servlet-API as servlet-container  
- For *JSON* operations, you need
  [*org.json:json:20160212*](https://www.1maven.com/findpomandjar/org.json:json:20160212)
  on your compile-classpath
  and you also have to include this JAR in the output WAR's lib\ directory.