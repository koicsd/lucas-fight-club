package entity;

import org.json.JSONObject;

/**
 * Created by KoicsD on 2016.07.05..
 */
public class User {

    String email;
    String password;
    JSONObject profile;

    public User(String email, String password, JSONObject profile) {
        this.email = email;
        this.password = password;
        this.profile = profile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JSONObject getProfile() {
        return profile;
    }

    public void setProfile(JSONObject profile) {
        this.profile = profile;
    }
}
