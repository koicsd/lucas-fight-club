package database;

/**
 * Created by KoicsD on 2016.07.05..
 */
public class UserSetException extends RuntimeException {

    public UserSetException() {
    }

    public UserSetException(String message) {
        super(message);
    }

    public UserSetException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserSetException(Throwable cause) {
        super(cause);
    }

    public UserSetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
