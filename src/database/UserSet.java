package database;

import entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KoicsD on 2016.07.05..
 */
public class UserSet {

    private List<User> users = new ArrayList<User>();

    public void addUser(User u) {
        if (containsUser(u.getEmail()))
            throw new DuplicateEmailException("The email '" + u.getEmail() + "' already exists in the UserSet.");
        users.add(u);
    }

    public boolean containsUser(String email) {
        for (User u: users) {
            if (u.getEmail().equals(email))
                return true;
        }
        return false;
    }

    public User getUser(String email) {
        for (User u: users) {
            if (u.getEmail().equals(email))
                return u;
        }
        throw new UserNotFoundException("The user with email '" + email + "' cannot be found in the UserSet.");
    }

    public User[] toArray(){
        return (User[]) users.toArray();
    }

}
