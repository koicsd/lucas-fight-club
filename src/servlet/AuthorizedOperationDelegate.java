package servlet;

import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpSession;


interface AuthorizedOperationDelegate {
    void invoke(HttpSession session, BufferedReader reader, PrintWriter writer);
}
