package servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.json.JSONException;
import org.json.JSONObject;


public class SessionHandler implements ServletContextListener, HttpSessionListener {

    private Map<String, String> loggedIn = new HashMap<>();

    private void signLogin(HttpSession session) {
        String email = (String)session.getAttribute("email");
        String id = session.getId();
        loggedIn.put(email, id);
        if (email != null) {
            System.err.printf("User with email '%s' has logged in.\n", email);
        }
    }

    public boolean checkIfLoggedIn(String email) {
        return loggedIn.containsKey(email);
    }

    public String getSessionId(String email) {
        if (!loggedIn.containsKey(email))
            return null;
        return loggedIn.get(email);
    }

    void tryLogin(SessionGetter sessionGetter, BufferedReader reader, PrintWriter writer) throws IOException
    {
        HttpSession session = sessionGetter.getSession(true);
        if (session.getAttribute("email") != null)
        {
            handleSessionAlreadyBeingUsed(session, writer);
            return;
        }
        try
        {
            JSONObject data = App.parseData(reader);
            String email = (String)data.get("email");
            String password = (String)data.get("password");
            if (App.users.containsUser(email) && App.users.getUser(email).getPassword().equals(password))
            {
                if (checkIfLoggedIn(email))
                {
                    handleAlreadyLoggedIn(session, writer);
                }
                else
                {
                    login(session, email, writer);
                }
            }
            else
            {
                handleAuthenticationFailed(session, writer);
            }
        }
        catch (JSONException | ClassCastException e)
        {
            handleInvalidData(session, writer);
        }
    }

    void tryAuthorizedOperation(SessionGetter sessionGetter, BufferedReader reader, PrintWriter writer,
                                       AuthorizedOperationDelegate delegate) {
        HttpSession session = sessionGetter.getSession(false);
        String currentUser = session != null ? (String) session.getAttribute("email") : null;
        if (currentUser == null || !App.users.containsUser(currentUser))
        {
            handleUnauthorizedAccess(session, writer);
            return;
        }
        delegate.invoke(session, reader, writer);
    }

    private void login(HttpSession session, String email, PrintWriter writer)
    {
        session.setAttribute("email", email);
        signLogin(session);
        JSONObject answer = new JSONObject();
        JSONObject successInfo = new JSONObject();
        successInfo.put("operation", "login");
        successInfo.put("email", email);
        successInfo.put("session", session.getId());
        answer.put("success", successInfo);
        writer.print(answer.toString());
    }

    void logout(HttpSession session, PrintWriter writer)
    {
        String email = (String)session.getAttribute("email");
        session.invalidate();
        JSONObject answer = new JSONObject();
        JSONObject successInfo = new JSONObject();
        successInfo.put("operation", "logout");
        successInfo.put("email", email);
        answer.put("success", successInfo);
        writer.print(answer.toString());
    }

    void confirmAlive(HttpSession session, PrintWriter writer) {
        String email = (String)session.getAttribute("email");
        JSONObject answer = new JSONObject();
        JSONObject successInfo = new JSONObject();
        successInfo.put("operation", "keep-alive");
        successInfo.put("email", email);
        successInfo.put("session", session.getId());
        answer.put("success", successInfo);
        writer.print(answer.toString());
    }

    private void handleSessionAlreadyBeingUsed(HttpSession session, PrintWriter writer) {
        JSONObject ans = new JSONObject();
        JSONObject err = new JSONObject();
        err.put("code", 1);
        err.put("message", "Your session is already in-use.");
        ans.put("error", err);
        writer.print(ans.toString());
    }

    private void handleAuthenticationFailed(HttpSession session, PrintWriter writer)
    {
        JSONObject ans = new JSONObject();
        JSONObject err = new JSONObject();
        err.put("code", 2);
        err.put("message", "Username or password invalid.");
        ans.put("error", err);
        writer.print(ans.toString());
    }

    private void handleAlreadyLoggedIn(HttpSession session, PrintWriter writer)
    {
        JSONObject ans = new JSONObject();
        JSONObject err = new JSONObject();
        err.put("code", 3);
        err.put("message", "You are already logged in (maybe in another session).");
        ans.put("error", err);
        writer.print(ans.toString());
    }

    private void handleUnauthorizedAccess(HttpSession session, PrintWriter writer)
    {
        JSONObject ans = new JSONObject();
        JSONObject err = new JSONObject();
        err.put("code", 4);
        err.put("message", "You are not logged in.");
        ans.put("error", err);
        writer.print(ans.toString());
        if (session != null)
            session.invalidate();
    }

    private void handleInvalidData(HttpSession session, PrintWriter writer)
    {
        JSONObject ans = new JSONObject();
        JSONObject err = new JSONObject();
        err.put("code", 5);
        err.put("message", "Request-content cannot be parsed.");
        ans.put("error", err);
        writer.print(ans.toString());
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().setAttribute(this.getClass().getName(), this);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }


    @Override
    public void sessionCreated(HttpSessionEvent se) {

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        String email = (String)session.getAttribute("email");
        if (email != null) {
            if (loggedIn.containsKey(email))
                loggedIn.remove(email);
            System.err.printf("User with email '%s' has logged out.\n", email);
        }
    }

}
