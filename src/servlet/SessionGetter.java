package servlet;

import javax.servlet.http.HttpSession;


interface SessionGetter {
    HttpSession getSession(boolean createNew);
}
