package servlet;


class ConfigurationException extends Exception {
    ConfigurationException() {
        super();
    }

    ConfigurationException(String message) {
        super(message);
    }

    ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    ConfigurationException(Throwable cause) {
        super(cause);
    }

    ConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
