package servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

// Servlet-API
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// 3rd-party: https://www.1maven.com/findpomandjar/org.json:json:20160212
import org.json.JSONException;
import org.json.JSONObject;

import database.UserSet;
import entity.User;


public class App extends HttpServlet
{	
	static UserSet users = new UserSet();
	
	static {
		JSONObject lucas = new JSONObject();
		lucas.put("name", "Lucas");
		lucas.put("age", "36");
		users.addUser(new User("lucas@example.com", "Asdf1234", lucas));
		
		JSONObject peter = new JSONObject();
		peter.put("name", "Peter");
		peter.put("age", "28");
		users.addUser(new User("peter@example.com", "Qwert", peter));
	}
	
	private interface TaskSwitcherDelegate
	{
		void invoke(SessionGetter sessionGetter, SessionHandler sessionHandler, String operation,
						   BufferedReader reader, PrintWriter writer) throws IOException, ConfigurationException;
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
	{
		doTask(req, resp, this::switchPostTask);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
	{
		doTask(req, resp, this::switchGetTask);
	}
	
	private void doTask(HttpServletRequest req, HttpServletResponse resp,
						TaskSwitcherDelegate delegate) throws IOException
	{
		getServletContext().setAttribute(getServletName(), this);
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		try (
				PrintWriter writer = resp.getWriter();
				BufferedReader reader = req.getReader()
			)
		{
			try {
				Object sessionHandler = getServletContext().getAttribute(SessionHandler.class.getName());
				if (sessionHandler == null || !(sessionHandler instanceof SessionHandler))
					throw new ConfigurationException(getServletName() +
							" cannot find " + SessionHandler.class.getName() + " instance");
				String operation = getInitParameter("operation");
				if (operation == null)
					throw new ConfigurationException("Initial parameter 'operation' not found");
				delegate.invoke(req::getSession, (SessionHandler) sessionHandler, operation, reader, writer);
			}
			catch (Exception e)
			{
				resp.sendError(500);
				e.printStackTrace();
			}
		}
	}
	
	private void switchPostTask(SessionGetter sessionGetter, SessionHandler sessionHandler, String operation,
								BufferedReader reader, PrintWriter writer) throws IOException, ConfigurationException
	{
		switch (operation)
		{
		case "login":
			sessionHandler.tryLogin(sessionGetter, reader, writer);
			break;
		case "logout":
			sessionHandler.tryAuthorizedOperation(sessionGetter, reader, writer,
					(HttpSession s, BufferedReader r, PrintWriter w) -> sessionHandler.logout(s, w)
			);
			break;
		default:
			throw new ConfigurationException("Invalid value for initial parameter 'operation' handling POST request: " +
					"'" + operation + "'");
		}
	}

	private void switchGetTask(SessionGetter sessionGetter, SessionHandler sessionHandler, String operation,
							   BufferedReader reader, PrintWriter writer) throws IOException, ConfigurationException
	{
		switch (operation)
		{
		case "get-profile":
			sessionHandler.tryAuthorizedOperation(sessionGetter, reader, writer,
					(HttpSession s, BufferedReader r, PrintWriter w) -> getProfile(s, w)
			);
			break;
		case "keep-alive":
			sessionHandler.tryAuthorizedOperation(sessionGetter, reader, writer,
					(HttpSession s, BufferedReader r, PrintWriter w) -> sessionHandler.confirmAlive(s, w)
			);
			break;
		default:
			throw new ConfigurationException("Invalid value for initial parameter 'operation' handling GET request: " +
					"'" + operation + "'");
		}
	}
	
	private void getProfile(HttpSession session, PrintWriter writer)
	{
		String email = (String)session.getAttribute("email");
		JSONObject answer = new JSONObject();
		JSONObject profile = users.getUser(email).getProfile();
		answer.put("email", email);
		answer.put("profile", profile);
		writer.print(answer.toString());
	}

	static JSONObject parseData(BufferedReader reader) throws IOException, JSONException
	{
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null)
		{
			sb.append(line);
		}
		return new JSONObject(sb.toString());
	}

}